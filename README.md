## steps to reproduce

```bash
npm ci

npm start

curl localhost:300

# we should get this:        {"hello":{},"should_be_true":true}
# but we got this instead:   {"should_be_true":false}
```

Play with [`src/app.module.ts`](./src/app.module.ts) file

