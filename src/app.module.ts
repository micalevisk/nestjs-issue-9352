import { Module, Inject, Injectable, Scope, Controller, Get } from '@nestjs/common';

const A = Symbol('A')

// @Injectable() // This works
@Injectable({ scope: Scope.REQUEST }) // This doesn't
export class DummyService {}

@Injectable()
export class AppService {
  @Inject() private readonly _: DummyService // If we drop this, it will work

  @Inject('foo') public readonly [A]: DummyService
  //                             \ /
  //                              +--> if this wasn't a Symbol, it would work too
}

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Get()
  hello() { 
    return {
      hello: this.appService[A],
      should_be_true: this.appService[A] !== undefined,
    }
  }
}


@Module({
  controllers: [AppController],
  providers: [
    AppService,
    DummyService,
    { provide: 'foo', useClass: DummyService },
  ],
})
export class AppModule {}
